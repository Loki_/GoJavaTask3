package Task01;

/**
 * Created by Loki_ on 13.11.16.
 */
public class Bird {

    private String str;
    public Bird(String str) {
        this.str = str;
    }

    public void sing() {
        System.out.println("A am " + str);
    }
}
