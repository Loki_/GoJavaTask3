package Task01;

/**
 * Created by Loki_ on 13.11.16.
 */
public class Task1 {

    public static void main(String[] args) {

        Bird b1 = new Bird("walking");
        Bird b2 = new Bird("flying");
        Bird b3 = new Bird("singing");
        Bird b4 = new Bird("Bird");

        b1.sing();
        b2.sing();
        b3.sing();
        b4.sing();
    }
}
