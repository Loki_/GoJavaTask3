package Task03;

/**
 * Created by Loki_ on 13.11.16.
 */
public class Student {

    private String firstName;
    private String lastName;
    private int group;
    private Course coursesTaken;
    private int age;

    public Student(String fName, String lName, int gr){
        this.firstName = fName;
        this.lastName = lName;
        this.group = gr;
    }

    public Student(String lName, Course cTaken){
        this.lastName = lName;
        this.coursesTaken = cTaken;
    }

    public String getFirstName(){
        return this.firstName;
    }
    public void setFirstName(String firstN){
        this.firstName = firstN;
    }

    public String getLastName(){
        return this.lastName;
    }
    public void setLastName(String lastN){
        this.lastName = lastN;
    }

    public int getGroup(){
        return this.group;
    }
    public void setGroup(int gr){
        this.group = gr;
    }

    public Course getCoursesTaken(){
        return this.coursesTaken;
    }
    public void setCoursesTaken(Course cour){
        this.coursesTaken = cour;
    }

    public int getAge(){
        return this.age;
    }
    public void setAge(int age){
        this.age = age;
    }
}
