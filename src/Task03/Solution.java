package Task03;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Loki_ on 13.11.16.
 */
public class Solution {
    public static void main(String[] args) throws ParseException {

        SimpleDateFormat startDate = new SimpleDateFormat("yyyy/MM/dd");
        Date startDay = startDate.parse("2013/09/01");

        Course course1 = new Course(startDay, "Java Core");
        Course course2 = new Course(48, "SQL", "Mr.Snow");
        Course course3 = new Course(startDay, "Spring");
        Course course4 = new Course(48, "Manul QA", "Mr. Alexandr");
        Course course5 = new Course(startDay, "JavaScript");

        Student student1 = new Student("Vaska", "Sidorov", 5);
        Student student2 = new Student("Masha", course1);

        CollegeStudent collegeStudent1 = new CollegeStudent("Karl", "Sadovsky", 5);
        CollegeStudent collegeStudent2 = new CollegeStudent("Sadovsky", course2);
        CollegeStudent collegeStudent3 = new CollegeStudent("Katya", "Lohn", 5, "Indiana State University", 2, 9128764);

        SpecialStudent specialStudent1 = new SpecialStudent("Woicek", "Rovinskiy", 5);
        SpecialStudent specialStudent2  = new SpecialStudent("Mark", "Slown", 5, "Stenford University", 2, 983213);
        SpecialStudent specialStudent3 = new SpecialStudent("Derek", "Bishop", 5, 904761);

    }
}
