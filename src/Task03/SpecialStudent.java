package Task03;

/**
 * Created by Loki_ on 13.11.16.
 */
public class SpecialStudent extends CollegeStudent {

    private long secretKey;
    private String email;

    public SpecialStudent(String fName, String lName, int gr){
        super(fName, lName, gr);
    }

    public SpecialStudent(String fName, String lName, int gr,String colName, int rat, long idd){
        super(fName, lName, gr, colName, rat, idd);
    }

    public SpecialStudent(String fName, String lName, int gr, long secKey){
        super(fName, lName, gr);
        this.secretKey = secKey;
    }

    public long getSecretKey(){
        return this.secretKey;
    }
    public void setSecretKey(long ss){
        this.secretKey = ss;
    }

    public String getEmail(){
        return this.email;
    }
    public void setEmail(String ee){
        this.email = ee;
    }
}
