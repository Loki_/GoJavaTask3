package Task03;

/**
 * Created by Loki_ on 13.11.16.
 */
public class CollegeStudent extends Student {

    private String collegeName;
    private int rating;
    private long id;

    public CollegeStudent(String fName, String lName, int gr){
        super(fName, lName, gr);
    }

    public CollegeStudent(String lName, Course cTaken){
        super(lName, cTaken);
    }

    public CollegeStudent(String fName, String lName, int gr, String colName, int rat, long idd){
        super(fName, lName, gr);
        this.collegeName = colName;
        this.rating = rat;
        this.id = idd;
    }

    public String getCollegeName(){
        return this.collegeName;
    }
    public void setCollegeName(String colName){
        this.collegeName = colName;
    }

    public int getRating(){
        return this.rating;
    }
    public void setRating(int rat){
        this.rating = rat;
    }

    public long getId(){
        return this.id;
    }
    public void setId(long dd){
        this.id = dd;
    }
}
