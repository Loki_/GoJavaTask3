package Task03;

import java.util.Date;

/**
 * Created by Loki_ on 13.11.16.
 */
public class Course {

    private Date startDate;
    private String name;
    private int hoursDuration;
    private String teacherName;

    public Course(Date date, String n){
        this.startDate = date;
        this.name = n;
    }

    public Course(int h, String n, String t){
        this.hoursDuration = h;
        this.name = n;
        this.teacherName = t;
    }

    public Date getStartDate(){
        return this.startDate;
    }
    public void setStartDate(Date date){
        this.startDate = date;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String n){
        this.name = n;
    }

    public int getHoursDuration(){
        return this.hoursDuration;
    }
    public void setHoursDuration(int h){
        this.hoursDuration = h;
    }

    public String getTeacherName(){
        return this.teacherName;
    }
    public void setTeacherName(String tName){
        this.teacherName = tName;
    }
}
