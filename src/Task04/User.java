package Task04;

/**
 * Created by Loki_ on 13.11.16.
 */
public class User {

    private String name;
    private int balance;
    private int monthsOfEmployment;
    private String companyName;
    private int salary;
    private String currency;

    public User(String n, int bal, int mOfE, String cN, int sal, String cur){
        this.name = n;
        this.balance = bal;
        this.monthsOfEmployment = mOfE;
        this.companyName = cN;
        this.salary = sal;
        this.currency = cur;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String na){
        this.name = na;
    }

    public int getBalance(){
        return this.balance;
    }
    public void setBalance(int bal){
        this.balance = bal;
    }

    public int getMonthsOfEmployment(){
        return this.monthsOfEmployment;
    }
    public void setMonthsOfEmployment(int monEm){
        this.monthsOfEmployment = monEm;
    }

    public String getCompanyName(){
        return this.companyName;
    }
    public void setCompanyName(String comNam){
        this.companyName = comNam;
    }

    public int getSalary(){
        return this.salary;
    }
    public void setSalary(int sal){
        this.salary = sal;
    }

    public String getCurrency(){
        return this.currency;
    }
    public void setCurrency(String cur){
        this.currency = cur;
    }

    public void paySalary(){
        int sum = balance + salary;
    }
    public int withdraw(int summ){
        int cash;
        if (summ < 1000){
            cash = summ + (summ/100*5);
        } else {
            cash = summ + (summ/100*10);
        }
        int res = balance - cash;
        return res;
    }
    public int companyNameLenght(String companyName){
        int lenght = companyName.length();
        return lenght;
    }
    public int monthIncreaser(int addMonth){
        int summMonths = monthsOfEmployment + addMonth;
        return summMonths;
    }
}
